pyzigbee is a library that lets to talk to Legrand zigbee devices through gateways

[![build status](https://gitlab.com/ci/projects/19413/status.png?ref=master)](https://gitlab.com/ci/projects/19413?ref=master)
[![Build status](https://ci.appveyor.com/api/projects/status/pxiciuj8ikefmx1b/branch/master?svg=true)](https://ci.appveyor.com/project/pierreroth/pyzigbee) [![Build status](https://readthedocs.org/projects/pyzigbee/badge/)](http://pyzigbee.readthedocs.org/en/latest/) [![Code Health](https://landscape.io/github/pierreroth/pyzigbee/master/landscape.svg?style=flat)](https://landscape.io/github/pierreroth/pyzigbee/master) [![Coverage Status](https://coveralls.io/repos/pierreroth/pyzigbee/badge.svg?branch=master)](https://coveralls.io/r/pierreroth/pyzigbee?branch=master)


Installation
------------

If you only want to install the library:

    python setup.py install

or with python installer:

    pip install .

Use the python installer to install other dependencies that may be needed to run tests or build documentation:

    pip install -r requirements.txt

Testing
-------

To run the full test suite, from the top directory:

    make test

Documentation
-------------

Latest documentattion can be found here: http://pyzigbee.readthedocs.org/en/latest/

Other formats and older versions: https://readthedocs.org/projects/pyzigbee/

To build the documentation:

    make doc

Contributing
------------

You may want to submit some pull requests. Make sure the following command runs without any error:

    make check
